#!/bin/bash

# todo: prompt for RTL version to be compiled, dl correct one
wget https://github.com/diederikdehaas/rtl8812AU/archive/driver-4.3.14.zip

# todo: scan for C compilers, prompt for user preference, default to gcc

# todo: check for archiving utils / make more cross-systemy
unar driver-4.3.14.zip

cd rtl8812AU-driver-4.3.14;

make CC=/usr/bin/gcc;
sudo make install;
sudo modprobe 8812au;
echo 8812au | sudo tee -a /etc/modules;
