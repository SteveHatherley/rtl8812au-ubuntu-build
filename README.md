# rtl8812AU build
This bash script grabs & compiles the rtl8812AU driver for the Broadcom rtl8812AU and rtl8821AU/rtl8811AU chipsets from [https://github.com/diederikdehaas/rtl8812AU](https://github.com/diederikdehaas/rtl8812AU)

